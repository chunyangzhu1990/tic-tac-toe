//
//  ViewController2.m
//  Lab3_Zhu
//
//  Created by Brother Chun on 9/26/14.
//  Copyright (c) 2014 ___Chunyang Zhu___. All rights reserved.
//

#import "ViewController2.h"
#import "ViewController3.h"
@interface ViewController2 ()

@end

@implementation ViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
 //   [super didChangeValueForKey:<#(NSString *)#>];
    
}
- (IBAction)buttonTouch:(id)sender {
    ViewController3* v3=[[ViewController3 alloc] initWithNibName:@"ViewController3" bundle: nil];
    [self.navigationController pushViewController:v3 animated:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
