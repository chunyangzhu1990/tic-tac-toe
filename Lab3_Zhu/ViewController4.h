//
//  ViewController4.h
//  Lab3_Zhu
//
//  Created by Brother Chun on 9/27/14.
//  Copyright (c) 2014 ___Chunyang Zhu___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController4 : UIViewController {
     int array[10];
     int array2[8][3];
}
@property (nonatomic, readwrite) NSInteger player;
@property (nonatomic, readwrite) NSInteger _round;
@property (nonatomic, readwrite) NSString*  result;
@property (nonatomic) NSInteger option1;
@property (nonatomic) NSInteger option2;

- (void)initial;
- (void)winner;
- (void)gameOver;


@end
