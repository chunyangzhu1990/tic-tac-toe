//
//  ViewController4.m
//  Lab3_Zhu
//
//  Created by Brother Chun on 9/27/14.
//  Copyright (c) 2014 ___Chunyang Zhu___. All rights reserved.
//
#import "ViewController2.h"
#import "ViewController3.h"
#import "ViewController4.h"

@interface ViewController4 ()
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UIButton *button3;
@property (weak, nonatomic) IBOutlet UIButton *button4;
@property (weak, nonatomic) IBOutlet UIButton *button5;
@property (weak, nonatomic) IBOutlet UIButton *button6;
@property (weak, nonatomic) IBOutlet UIButton *button7;
@property (weak, nonatomic) IBOutlet UIButton *button8;
@property (weak, nonatomic) IBOutlet UIButton *button9;

@end

@implementation ViewController4

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initial];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
         ViewController3* v3=[[ViewController3 alloc] initWithNibName:@"ViewController3" bundle: nil];
        [self.navigationController pushViewController:v3 animated:true];
        
        
        for (int j = 0; j < 10; j++ )
        {
            array[j]=0;
        }
        [self initial];

        
    }
    else{
        
    }
}
@synthesize player;

- (void)initial {
    
    array2[0][0] = 1;
    array2[0][1] = 2;
    array2[0][2] = 3;
    array2[1][0] = 4;
    array2[1][1] = 5;
    array2[1][2] = 6;
    array2[2][0] = 7;
    array2[2][1] = 8;
    array2[2][2] = 9;
    array2[3][0] = 1;
    array2[3][1] = 4;
    array2[3][2] = 7;
    array2[4][0] = 2;
    array2[4][1] = 5;
    array2[4][2] = 8;
    array2[5][0] = 3;
    array2[5][1] = 6;
    array2[5][2] = 9;
    array2[6][0] = 1;
    array2[6][1] = 5;
    array2[6][2] = 9;
    array2[7][0] = 3;
    array2[7][1] = 5;
    array2[7][2] = 7;
    int value = arc4random() % 2+1;
    
 if(self.option2==0)
    self.player = 1;
 else if(self.option2==1)
    self.player=2;
 else if(self.option2==2)
    self.player = value;
    
    if (player == 2) {
    if (self.option1==0) {
        [self aI];
    }
    }
    
    
    }

- (void)gameOver {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:_result message:@"DO You Want Try Again ?" delegate:self cancelButtonTitle:@"Restart" otherButtonTitles:nil
  ];

    [alert show];
}

-(void)aI{
    
    int decision = -1;
    for (int i=0;i<8;i++){
        int p1=array2[i][0];
        int p2=array2[i][1];
        int p3=array2[i][2];
        if(array[p1]==2&&array[p2]==2&&array[p3]==0) {
            decision = p3;
        }
        else if(array[p1]==0&&array[p2]==2&&array[p3]==2){
            decision=p1;
        }
        else if(array[p1]==2&&array[p2]==0&&array[p3]==2){
            decision=p2;
        }
    }
    
    if(decision==-1){
        for (int i=0;i<8;i++){
            int p1=array2[i][0];
            int p2=array2[i][1];
            int p3=array2[i][2];
            if(array[p1]==1&&array[p2]==1&&array[p3]==0) {
                decision = p3;
            }
            else if(array[p1]==0&&array[p2]==1&&array[p3]==1){
                decision=p1;
            }
            else if(array[p1]==1&&array[p2]==0&&array[p3]==1){
                decision=p2;
            }

        }
    }
    
    if (decision==-1) {
        if(array[5]==0){
            decision=5;
        }
        else{
            for(int i=1;i<10;i++){
                if(array[i]==0)
                    decision=i;
            
            }
        }
    }
    
    switch (decision) {
        case 1:
            [self buttonOne:self.button1];
            break;
        case 2:
            [self buttonOne:self.button2];
            break;
        case 3:
            [self buttonOne:self.button3];
            break;
        case 4:
            [self buttonOne:self.button4];
            break;
        case 5:
            [self buttonOne:self.button5];
            break;
        case 6:
            [self buttonOne:self.button6];
            break;
        case 7:
            [self buttonOne:self.button7];
            break;
        case 8:
            [self buttonOne:self.button8];
            break;
        case 9:
            [self buttonOne:self.button9];
            break;
    }
    
    
    
}

- (void)winner{
    if((array[1]==array[2])&&(array[2]==array[3])){
        if(array[1]==1){
        self.result=@"Player 1 is winner !";
            [self gameOver];
        }
        else if(array[1]==2){
            self.result=@"Player 2 is winner !";
            [self gameOver];        }
    }
    else if((array[4]==array[5])&&(array[5]==array[6])){
        if(array[4]==1){
        self.result=@"Player 1 is winner !";
            [self gameOver];
        }
        else if(array[4]==2){
            self.result=@"Player 2 is winner !";
            [self gameOver];        }
    }
    else if((array[7]==array[8]&&array[8]==array[9])){
        if(array[7]==1){
            self.result=@"Player 1 is winner !";
            [self gameOver];
        }
        else if(array[7]==2){
            self.result=@"Player 2 is winner !";
            [self gameOver];        }
    }
    else if((array[1]==array[4]&&array[4]==array[7])){
        if(array[1]==1){
            self.result=@"Player 1 is winner !";
            [self gameOver];
        }
        else if(array[1]==2){
            self.result=@"Player 2 is winner !";
            [self gameOver];        }
    }
    else if((array[2]==array[5]&&array[5]==array[8])){
        if(array[2]==1){
            self.result=@"Player 1 is winner !";
        [self gameOver];
        }
        else if(array[2]==2){
            self.result=@"Player 2 is winner !";
            [self gameOver];        }
    }
    else if((array[3]==array[6]&&array[6]==array[9])){
        if(array[3]==1){
            self.result=@"Player 1 is winner !";
        [self gameOver];
        }
        else if(array[3]==2){
            self.result=@"Player 2 is winner !";
            [self gameOver];        }
    }
    else if((array[1]==array[5]&&array[5]==array[9])){
        if(array[1]==1){
            self.result=@"Player 1 is winner !";
            [self gameOver];
        }
           
        else if(array[1]==2){
            self.result=@"Player 2 is winner !";
            [self gameOver];
        }
    }
    else if((array[3]==array[5]&&array[5]==array[7])){
        if(array[3]==1){
            self.result=@"Player 1 is winner !";
            [self gameOver];
        }
        else if(array[3]==2){
            self.result=@"Player 2 is winner !";
            [self gameOver];
        }
    }
    else{
      if(array[1]!=0&&array[2]!=0&&array[3]!=0&&array[4]!=0&&array[5]!=0&&array[6]!=0&&array[7]!=0&&array[8]!=0&&array [9]!=0){
        // NSLog(@"size:",sizeof(array));
        self.result=@"Draw";
        [self gameOver];
        }
    else{}
    }
}


- (IBAction)buttonOne:(id)sender {
  
    UIButton *button = sender;
    NSLog(@"%ld",(long)player);
    if (player==1) {
    [button setTitle:@"X" forState:UIControlStateNormal];
        player=2;
        
        
    }
    else if(player==2){
    [button setTitle:@"O" forState:UIControlStateNormal];
        player=1;
    }
    if(button==self.button1){
        __round=1;
        [button setEnabled:NO];
        if([button.currentTitle isEqual:@"X"]){
            array[1]=1;
        }
        else if ([button.currentTitle isEqual:@"O"]){
            array[1]=2;
        }
    }
    else if(button==self.button2){
        __round=2;
        [button setEnabled:NO];
        if([button.currentTitle isEqual:@"X"]){
            array[2]=1;
        }
        else if ([button.currentTitle isEqual:@"O"]){
            array[2]=2;
        }
    }
    else if(button==self.button3){
        __round=3;
        [button setEnabled:NO];
        if([button.currentTitle isEqual:@"X"]){
            array[3]=1;
        }
        else if ([button.currentTitle isEqual:@"O"]){
            array[3]=2;
        }
    }
    else if(button==self.button4){
        __round=4;
        [button setEnabled:NO];
        if([button.currentTitle isEqual:@"X"]){
            array[4]=1;
        }
        else if ([button.currentTitle isEqual:@"O"]){
            array[4]=2;
        }
    }
    else if(button==self.button5){
        __round=5;
        [button setEnabled:NO];
        if([button.currentTitle isEqual:@"X"]){
            array[5]=1;
        }
        else if ([button.currentTitle isEqual:@"O"]){
            array[5]=2;
        }
    }
    else if(button==self.button6){
        __round=6;
        [button setEnabled:NO];
        if([button.currentTitle isEqual:@"X"]){
            array[6]=1;
        }
        else if ([button.currentTitle isEqual:@"O"]){
            array[6]=2;
        }
    }
    else if(button==self.button7){
        __round=7;
        [button setEnabled:NO];
        if([button.currentTitle isEqual:@"X"]){
            array[7]=1;
        }
        else if ([button.currentTitle isEqual:@"O"]){
            array[7]=2;
        }
    }
    else if(button==self.button8){
        __round=8;
        [button setEnabled:NO];
        if([button.currentTitle isEqual:@"X"]){
            array[8]=1;
        }
        else if ([button.currentTitle isEqual:@"O"]){
            array[8]=2;
        }
    }
    else if(button==self.button9){
        __round=9;
        [button setEnabled:NO];
        if([button.currentTitle isEqual:@"X"]){
            array[9]=1;
        }
        else if ([button.currentTitle isEqual:@"O"]){
            array[9]=2;
        }
    }
    
    for (int j = 0; j < 10; j++ )
    {
        NSLog(@"Element[%d] = %d\n", j, array[j] );
    }
    NSLog(@" The button's title is %@.",button.currentTitle);
    [self winner];
    
    if (player == 2) {
        
        if (self.option1==0) {
            [self aI];
        }
    }
    
}






    
@end
