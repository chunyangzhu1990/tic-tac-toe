//
//  ViewController3.m
//  Lab3_Zhu
//
//  Created by Brother Chun on 9/26/14.
//  Copyright (c) 2014 ___Chunyang Zhu___. All rights reserved.
//

#import "ViewController3.h"
#import "ViewController4.h"
@interface ViewController3 ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *seg;
@property (weak, nonatomic) IBOutlet UISegmentedControl *seg2;

@end

@implementation ViewController3

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)startGame:(id)sender {
    NSInteger option1,option2;
    option1=self.seg.selectedSegmentIndex;
    option2=self.seg2.selectedSegmentIndex;
    ViewController4* v4=[[ViewController4 alloc] initWithNibName:@"ViewController4" bundle: nil];
    v4.option1 = option1;
    v4.option2 = option2;
    [self.navigationController pushViewController:v4 animated:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
